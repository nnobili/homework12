import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class Wxhw
{
	public String getWx(String zip)
	{
		JsonElement jse = null;
    String wxReport = null;
    String answer = null;

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.wunderground.com/api/3f5461b36fe4ee9b/conditions/q/"
					+ zip
					+ ".json");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
         //System.out.println("length" + jse.getAsJsonObject().get("response").getAsString().length);
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}
      
                      
      if (jse != null)
		{
      String check = "{\"version\":\"0.1\",\"termsofService\":\"http://www.wunderground.com/weather/api/d/terms.html\",\"features\":{\"conditions\":1}}";
      String current = jse.getAsJsonObject().get("response").toString();
      
      if (current.equals(check))
      {
      // Build a weather report
      String location = jse.getAsJsonObject().get("current_observation")
                           .getAsJsonObject().get("display_location")
                           .getAsJsonObject().get("full").getAsString();
      wxReport = "Location:      " + location + "\n";

      String time = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("observation_time").getAsString();
      wxReport = wxReport + "Time:          " + time + "\n";

      String weather = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("weather").getAsString();
      wxReport = wxReport + "Weather:       " + weather + "\n";
      
      String temp = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("temp_f").getAsString();
      wxReport = wxReport + "Temperature F: " + temp + "\n";

      String wind_dir = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("wind_dir").getAsString();
      String wind_mph = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("wind_mph").getAsString();
      String wind_gust_mph = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("wind_gust_mph").getAsString();
      wxReport = wxReport + "Wind:          " + "From the " + wind_dir + " at " + wind_mph + " MPH Gusting to " + wind_gust_mph + " MPH" +"\n";
      
      String pressure = jse.getAsJsonObject().get("current_observation")
                       .getAsJsonObject().get("pressure_in").getAsString();
      wxReport = wxReport + "Pressure inHG: " + pressure + "\n";
      }
      else
      {
      wxReport = "ERROR: No cities match your search query";
      }
		}
    return wxReport;
	}

	public static void main(String[] args)
	{
		Wxhw b = new Wxhw();
    if ( args.length == 0 )
      System.out.println("Please enter a US Zip code as the first argument.");
    else
    {
		  String wx = b.getWx(args[0]);
      if ( wx != null )
		    System.out.println(wx);
    }
	}
}
